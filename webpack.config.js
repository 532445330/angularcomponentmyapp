const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack =require('webpack');
module.exports = {
    context: path.resolve(__dirname,'src'),
    entry: {
        dataMining:'./dataMining/app.js',
        statistical:'./statistical/app.js'
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath:'/'
    },
    module: {
        rules:[
            {
                test: /\.(js|jsx)$/,
                use: {
                    loader:'babel-loader'
                },
                exclude:/node_modules|public/
            },
            {
                test: /\.css$/,
                use: ['style-loader','css-loader'],
                exclude:/public/
            },
            {
                test:/\.scss$/,
                use:[
                    {
                        loader:"style-loader"
                    },
                    {
                        loader:"css-loader",
                        options:{
                            sourceMap: true
                        }
                    },
                    {
                        loader:"sass-loader",
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|eot|ttf|otf)$/,
                use: ['file-loader'],
                exclude:/public/
            },
            {
                test: /\.html$/,
                use: ['html-loader']
            }
        ]

    },
    devtool: 'inline-source-map',
    plugins:[
        new CleanWebpackPlugin(['dist']),
        new webpack.ProvidePlugin({
            $:"jquery",
            jQuery:"jquery",
            echarts:'echarts'
        }),
        new HtmlWebpackPlugin({
            filename:'dataMining.html',
            template: path.resolve(__dirname,'./src/dataMining/index.html'),
            title:'数据挖掘',
            chunksSortMode: 'dependency', //允许控制在将块包含到HTML之前应如何对块进行排序
            chunks:['dataMining']
        }),
        new HtmlWebpackPlugin({
            filename:'statistical.html',
            template: path.resolve(__dirname,'./src/statistical/index.html'),
            title:'统计信息',
            chunksSortMode: 'dependency',     //允许控制在将块包含到HTML之前应如何对块进行排序
            chunks:['statistical']              //允许您仅添加一些块
        })
    ],
    devServer: {
        // Base path for the serve content.
        contentBase: 'dist',
        hot:true,
        // historyApiFallback: true,
        openPage: 'statistical.html',
        publicPath:'/',
        port: 8080
    },
};