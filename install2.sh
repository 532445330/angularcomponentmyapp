#!/bin/bash
tar -xzvf yasm-1.3.0.tar.gz
cd yasm-1.3.0
./configure
make
make install
cd ../
tar -xzvf lame-3.99.5.tar.gz
cd lame-3.99.5
./configure
make
make install
cd ../
tar -xzvf opencore-amr-0.1.3.tar.gz
cd opencore-amr-0.1.3
./configure
make
make install
cd ../
tar -xjvf amrnb-11.0.0.0.tar.bz2
cd amrnb-11.0.0.0
./configure
make
make install
cd ../
tar -xjvf amrwb-11.0.0.0.tar.bz2
cd amrwb-11.0.0.0
./configure
make
make install
cd ../
tar -xjvf ffmpeg-2.5.3.tar.bz2
cd ffmpeg-2.5.3
./configure --enable-libmp3lame --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-version3 --enable-shared
make
make install
cd ../
echo "end"