'use script';
var express = require('express');
var router = express.Router();
var fs = require('fs');
// var Promise = require('bluebird');
var spawn = require('child_process').spawn;
var os = require('os');
var path = require('path');
var IP=getIPAdress();
    function getIPAdress(){
    var interfaces = require('os').networkInterfaces();
    for(var devName in interfaces){
        var iface = interfaces[devName];
        for(var i=0;i<iface.length;i++){
            var alias = iface[i];
            if(alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal){
                return alias.address;
            }
        }
    }
}
console.log(IP);
/* GET home page. */
router.get('/', function(req, res, next) {
    console.log('进入了/');
    // var Rstream = fs.createReadStream('data/backup/test.zip');
    // var Wstream = fs.createWriteStream('data/backup/test1.zip');
    // Rstream.pipe(Wstream);
    // fs.watch('data/backup',function(e,f){
    //     watchFolder.close();
    //     fs.watchFile('data/backup/'+f,function(curr,prev){
    //         console.log(curr.size,prev.size);
    //         console.log('test有变化');
    //     })
    //
    // })
    function copyFileContent(from, to) {
        return fs.readFile(from).then( (buffer) => {
            return fs.writeFile(to, buffer);
        });
    }

    // Promise.try( () => {
    //     console.log('111');
    //     return copyFileContent('data/backup/test.txt', 'data/backup/test1.txt');
    // }).then( () => {
    //     console.log('成功');
    //     console.log('success');
    // }).catch( (err) => {
    //     console.log('报错');
    //     console.error(err);
    // });
  res.render('index');
});
router.post('/test',function(req,res,next){
    console.log(a);
})

router.post('/getAudio',function(req,res){
    let path = req.body.path;
    let pathArr = path.split('/');
    let fileName = pathArr.splice(pathArr.length-1,1)[0];
    let file = getFormat(fileName);
    path = pathArr.join('/');
    console.log(path,fileName,file);
    var read = fs.createReadStream('data/'+path+'/'+fileName, { start: 1});
    var write = fs.createWriteStream('data/'+path+'/'+file.fileName+'_2.amr');
    read.pipe(write);
    read.on('close',()=>{
        fs.unlink('data/'+path+'/'+fileName);
        changePCM('data/'+path+'/'+file.fileName+'_2.amr','data/'+path+'/'+file.fileName,function(){
            changeAMR('data/'+path+'/'+file.fileName+'_2.pcm','data/'+path+'/'+file.fileName,function(){
                fs.unlinkSync('data/'+path+'/'+file.fileName+'_2.amr');
                fs.unlinkSync('data/'+path+'/'+file.fileName+'_2.pcm');
                res.send({
                    status:0,
                    path:path+'/'+file.fileName+'.wav'
                })
            });
        });

    })

})
function getFormat(n){
    console.log(n);
    var arr= n.split('.');
    var format = arr.splice(arr.length-1,1)[0];
    return {fileName:arr.join('.'),format:format};
}

function changePCM(path,targetPath,res){
    exe('decoder',[path,targetPath+'_2.pcm'],res);
}
function changeAMR(path,targetPath,res){
    //ffmpeg -f s16le -ar 24000 -i /path/to/pcm -f wav /path/to/wav
    exe('ffmpeg',['-f','s16le','-ar','24000','-i',path,targetPath+'.wav'],res);
}
function exe(cmd,command,res) {

    var cmd = spawn(cmd,command);
    cmd.stdout.setEncoding('ASCII');
    cmd.stdout.on('data', function (data) {
    });
    cmd.stderr.on('data', function (data) {
        console.log('-------------------------------');
        console.log('stderr:' + data);
        console.log('-------------------------------');
        console.log('stderr');
        console.log('data'+data);
        return;
    });
    cmd.on('exit', function (code) {
        console.log('exit');
        console.log('exited with code:' + code);
        console.log('-------------------------------');
        res();
    });
}
router.get('/toOne',(req,res)=>{
    res.render('toOne');
})
router.get('/page',(req,res)=>{
    res.render('page');
})



module.exports = router;
