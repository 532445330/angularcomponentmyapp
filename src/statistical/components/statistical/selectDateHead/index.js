/**
 * Created by wanglianxin on 2018/11/15.
 */
let tem = require('./selectDateHead.html');
require('./selectDateHead.scss');
class ctrl {
    constructor($state,$stateParams,$timeout,publicFn,service) {
        let _this =this;
        this.state=$state;
        this.stateParams=$stateParams;
        console.log(this.stateParams);
        this.publicFn=publicFn;
        this.service=service;
        this.setMonthIndex=null;
        this.timeout=$timeout;
        this.datePluginStart=null;
        this.datePluginEnd=null;
        this.timeout(()=>{
            if(typeof(this.data.defaultIndex) == 'number'){
                this.setMonthIndex=this.data.defaultIndex;
            }
        })
    }
    setDate(value,index){
        let data;
        if(this.setMonthIndex==index)return;
        this.setMonthIndex=index;
        console.log(this.service.getDate());
        if(value=='date'||value=='month'){
            let date = angular.fromJson(this.service.getDate('ioStatisOption'));

            console.log(date);
            if(date){
                console.log(value);
                if(value=='date'){
                    console.log(date);
                    data={
                        start:date.start,
                        end:date.end
                    }
                }
                else if(value=='month'){
                    let startArr= date.start.split('-');
                    startArr.pop();
                    let endArr = date.end.split('-');
                    endArr.pop();
                    data={
                        start:startArr.join('-'),
                        end:endArr.join('-')
                    }
                }
            }else{
                data={
                    start:'',
                    end:''
                }
            }
        }else{
            let time = this.getCurTime();
            this.startTime=time.time;
            let targetTime=this.getTargetTime(time,value);
            this.endTime=targetTime;
            data={
                start:this.startTime,
                end:this.endTime
            };
        }
        // TODO 这里需要记录 二次点击加载需要写全路径 解决$state.go不执行问题
        this.state.go(this.data.stateUrl,{[this.data.optionName]:angular.toJson(data)},{reload:this.data.stateUrl});

    };
    getCurTime(){
        let date = new Date();
        let [y,mo,d]=[date.getFullYear(),this.publicFn.toDou(date.getMonth()+1),this.publicFn.toDou(date.getDate())];
        return {year:y,month:mo,date:d,time:`${y}-${mo}-${d}`};
    }
    getTargetTime(time,value){
        let [y,mo,d]=[time.year,time.month,time.date];
        let month=mo+Number(value);
        if(month>12){
            y++;
            month=month-12;
        }
        return `${y}-${month}-${d}`;
    }
}
ctrl.$inject=['$state','$stateParams','$timeout','publicFn','service'];
module.exports = angular.module('selectdateHead', [])
    .component('selectdateHead', {
        template: tem,
        controller: ctrl,
        controllerAs: 'selectdateHeadCtrl',
        bindings:{
            data:'<'
        }
    })
    .name;