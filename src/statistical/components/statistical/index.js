/**
 * Created by wanglianxin on 2018/11/9.
 */
let tem = require('./statistacal.html');
let con = require('./statisticalContainer');
let selectDateHead = require('./selectDateHead');

require('./statistical.scss');

class ctrl {
    constructor($scope,$state,$stateParams,http,$location,$cookie){
        console.log(http);
        $scope.containerHeight=null;
        this.topType='';
        this.subType='';
        this.sourceId='';
        this.sourceName='';
        this.switchAside=true;   //开始显示mini
        $(window).on('load resize',()=>{
            let height = Number($(window).height())-114;
            if(height<543){
                height=543;
            }
            $scope.containerHeight=height;

            $scope.$apply();
        });
        $scope.navList=[
            {
                name:'人物画像',
                value:'people-info',
                type:'peopleInfo',
                show:true,
                subMenu:[
                    {
                        name:'基础信息',
                        value:'baseInfo'
                    },
                    {
                        name:'手机信息',
                        value:'phoneInfo'
                    },
                    {
                        name:'好友信息',
                        value:'friendInfo'
                    },
                    {
                        name:'经济信息',
                        value:'moneyInfo'
                    },
                    {
                        name:'采集信息',
                        value:'pickupInfo'
                    },
                    {
                        name:'关注信息',
                        value:'attentionInfo'
                    },
                    ]
            },
            {
                name:'案件排查',
                value:'case-check',
                type:'caseCheck',
                show:false,
                subMenu:[
                ]
            },
            {
                name:'词库碰撞',
                value:'lexicon-crash',
                type:'lexiconCrash',
                show:false,
                subMenu:[]
            },
            {
                name:'团伙分析',
                value:'gang-check',
                type:'gangCheck',
                show:false,
                subMenu:[]
            },
            {
                name:'时序分析',
                value:'time-check',
                type:'timeCheck',
                show:false,
                subMenu:[]
            },
            {
                name:'人物关系分析',
                value:'people-relation',
                type:'peopleRelation',
                show:false,
                subMenu:[]
            },
            {
                name:'活动区域分析',
                value:'activity-area',
                type:'activityArea',
                show:false,
                subMenu:[]
            },
            {
                name:'经济收支分析',
                value:'money-io',
                type:'moneyIo',
                show:false,
                subMenu:[]
            },
            {
                name:'采集数据统计',
                value:'collect-data',
                type:'collectData',
                show:false,
                subMenu:[]
            }
        ];
        this.materialsList=[
            {
                name:'美亚报表',
                tel:'13333333333',
                id:'美亚仿真_eb103997-049f-4d06-b0b7-41bf53ad03cc',
                imgUrl:'./images/u105.png'
            },
            {
                name:'iphoneX',
                tel:'13335656767',
                id:'fsadfdsgfdgfd2',
                imgUrl:'./images/u188.png'
            },
            {
                name:'oppo r17',
                tel:'13333765567',
                id:'fsadfdsgfdgfd3',
                imgUrl:'./images/u6.png'
            },
            {
                name:'vivo X23',
                tel:'13333333656',
                id:'fsadfdsgfdgfd4',
                imgUrl:'./images/u188.png'
            },
            {
                name:'小米8',
                tel:'13333333333',
                id:'fsadfdsgfdgfd5',
                imgUrl:'./images/u105.png'
            },
            {
                name:'iphoneX',
                tel:'13335656767',
                id:'fsadfdsgfdgfd6',
                imgUrl:'./images/u188.png'
            },
            {
                name:'oppo r17',
                tel:'13333765567',
                id:'fsadfdsgfdgfd7',
                imgUrl:'./images/u6.png'
            },
            {
                name:'vivo X23',
                tel:'13333333656',
                id:'fsadfdsgfdgfd8',
                imgUrl:'./images/u188.png'
            },
            {
                name:'小米8',
                tel:'13333333333',
                id:'fsadfdsgfdgfd9',
                imgUrl:'./images/u105.png'
            },
            {
                name:'iphoneX',
                tel:'13335656767',
                id:'fsadfdsgfdgfd10',
                imgUrl:'./images/u188.png'
            },
            {
                name:'oppo r17',
                tel:'13333765567',
                id:'fsadfdsgfdgfd11',
                imgUrl:'./images/u6.png'
            },
            {
                name:'vivo X23',
                tel:'13333333656',
                id:'fsadfdsgfdgfd12',
                imgUrl:'./images/u188.png'
            }
        ];

        http.post(        //临时获取案件列表
            'http://21.26.96.109:8080/jwcloud/html/getStatements',
            {
                dogId:'369166805',
                keyword:'',
                page: 1,
                class: 'all'
            },
            (data)=>{
                console.log(data);
            },
            (err)=>{
                console.log(err);
            }
        );

        this.topType=$scope.navList[0].type;
        this.subType=$location.path().split('/')[2]?$location.path().split('/')[2]:$scope.navList[0].subMenu[0].value;
        this.sourceId=$stateParams.sourceId?$stateParams.sourceId:this.materialsList[0].id;
        this.sourceName=$stateParams.sourceId?$stateParams.sourceId:this.materialsList[0].name;
        console.log(this.subType);
        $cookie.put('currentSourceName',this.sourceName);
        $cookie.put('currentSourceId',this.sourceId);
        $state.go('.'+this.subType,{sourceId:this.sourceId}); //默认跳转

    }
    loadImage(url){
        return require(''+url);
    };
}

ctrl.$inject = ["$scope","$state","$stateParams","http","$location","$cookies"];
module.exports = angular.module('statistical', [con,selectDateHead])
    .component('statistical',{
        template:tem,
        controller:ctrl,
        controllerAs:'statisticalCtrl'
    })
    .name;