/**
 * Created by wanglianxin on 2018/11/10.
 */
let peopleInfoTemplate = require('./peopleInfoTemplate');

module.exports = angular.module('statisticalContainer', [
    peopleInfoTemplate
]).name;