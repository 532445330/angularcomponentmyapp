/**
 * Created by wanglianxin on 2018/11/10.
 */
let conHead = require('./materialsList');
let baseInfo = require('./baseInfo');
let phoneInfo = require('./phoneInfo');
let friendInfo = require('./friendInfo/');
let moneyInfo = require('./moneyInfo');
let pickupInfo = require('./pickupInfo');
let attentionInfo = require('./attentionInfo');

require('../../statistical.scss');
class ctrl {
    constructor($scope,$stateParams,$location) {
        let _this=this;

        this.type2=$stateParams.sub;
        this.type1='peopleInfo';
    }
}
ctrl.$inject = ["$scope","$stateParams","$location"];
module.exports = angular.module('peopleInfo', [
    conHead,
    baseInfo,
    phoneInfo,
    friendInfo,
    moneyInfo,
    pickupInfo,
    attentionInfo
]).name;