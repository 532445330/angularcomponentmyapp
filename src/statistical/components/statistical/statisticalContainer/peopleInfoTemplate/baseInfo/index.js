/**
 * Created by wanglianxin on 2018/11/9.
 */
let tem = require('./peopleInfo.html');
require('./style.scss');
class ctrl {
    constructor($stateParams) {

    }
}
ctrl.$inject=['$stateParams'];
module.exports = angular.module('statisticalCon.baseInfo', [])
    .component('baseInfo',{
        template:tem,
        controller:ctrl,
        controllerAs:'ctrl'
    })
    .name;