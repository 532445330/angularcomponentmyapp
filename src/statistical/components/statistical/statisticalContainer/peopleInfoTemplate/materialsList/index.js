/**
 * Created by wanglianxin on 2018/11/10.
 */
let tem = require('./head.html');
require('./style.scss');
class ctrl {
    constructor($state,$stateParams,$window,$timeout) {
        let _this = this;
        this.showCtrl=false;   //是否展示子目录
        this.haveLeft=false;    //左边上有数据
        this.haveRight=false;       //右边有数据
        this.listCurrentIndex=1;        //目录当前最左边的index
        this.listAllIndex=1;            //目录一共多少页
        this.dataTranslateX=0;          //ul中数据平移距离
        this.currentDataIndex=0;       //当前数据索引
        function initListStyle(){
            let container = $('#materials-list-content');
            let listNum = _this.materialsList.length;
            let dataWidth = 94 * listNum;
            $('#materials-list-content ul').css({'width': dataWidth});
            let containerWidth = container.width();

            if (dataWidth > containerWidth) {
                _this.showCtrl = true;
                container.css({'padding-right': '117px'});
            } else {
                _this.showCtrl = false;
                container.css({'padding-right': '20px'});
            }

            limitNum = Math.floor(container.width() / 94);
            _this.listAllIndex = Math.ceil(listNum / limitNum);
            initMaterials();
        }

        $(window).on('load resize',()=>{
            initListStyle();
        });
        $timeout(()=>{
            initListStyle();
        },0);

        let limitNum=0; //一页能装多少

        function moveData(index){
            _this.dataTranslateX= 'translateX(-'+($('#materials-list-content ul li').eq(limitNum*(index-1)).position().left-20)+'px)';
        }
        function initMaterials(){
            _this.listCurrentIndex=1;
            moveData(_this.listCurrentIndex);
            checkIndex();
        }
        this.toLeft=function(){
            let num = _this.listCurrentIndex;
            if(num>1){
                _this.listCurrentIndex--;
                moveData(_this.listCurrentIndex);
            }
            checkIndex();
        };
        this.toRight = function(){
            if(_this.listCurrentIndex<_this.listAllIndex){
                _this.listCurrentIndex++;
                moveData(_this.listCurrentIndex);
            }
            checkIndex();
        };

        function checkIndex(){
            if(_this.listCurrentIndex==1){
                _this.haveLeft=false;
                _this.haveRight=true;
            }
            else if(_this.listCurrentIndex==_this.listAllIndex){
                _this.haveLeft=true;
                _this.haveRight=false;
            }
        }
        this.materialsList=[
            {
                name:'小米8',
                tel:'13333333333',
                id:'fsadfdsgfdgfd1',
                imgUrl:'u105.png'
            },
            {
                name:'iphoneX',
                tel:'13335656767',
                id:'fsadfdsgfdgfd2',
                imgUrl:'u188.png'
            },
            {
                name:'oppo r17',
                tel:'13333765567',
                id:'fsadfdsgfdgfd3',
                imgUrl:'u6.png'
            },
            {
                name:'vivo X23',
                tel:'13333333656',
                id:'fsadfdsgfdgfd4',
                imgUrl:'u188.png'
            },
            {
                name:'小米8',
                tel:'13333333333',
                id:'fsadfdsgfdgfd5',
                imgUrl:'u105.png'
            },
            {
                name:'iphoneX',
                tel:'13335656767',
                id:'fsadfdsgfdgfd6',
                imgUrl:'u188.png'
            },
            {
                name:'oppo r17',
                tel:'13333765567',
                id:'fsadfdsgfdgfd7',
                imgUrl:'u6.png'
            },
            {
                name:'vivo X23',
                tel:'13333333656',
                id:'fsadfdsgfdgfd8',
                imgUrl:'u188.png'
            },
            {
                name:'小米8',
                tel:'13333333333',
                id:'fsadfdsgfdgfd9',
                imgUrl:'u105.png'
            },
            {
                name:'iphoneX',
                tel:'13335656767',
                id:'fsadfdsgfdgfd10',
                imgUrl:'u188.png'
            },
            {
                name:'oppo r17',
                tel:'13333765567',
                id:'fsadfdsgfdgfd11',
                imgUrl:'u6.png'
            },
            {
                name:'vivo X23',
                tel:'13333333656',
                id:'fsadfdsgfdgfd12',
                imgUrl:'u188.png'
            },

        ];
        this.loadImage = function(image) {      //解决img标签src
            return require('../../../images/'+image);
        };
        $timeout(()=>{
            $state.go('.'+this.type2,{id:this.materialsList[0].id});
        },0);

    }
}
ctrl.$inject = ['$state','$stateParams','$window','$timeout'];
module.exports = angular.module('materialsList', [])
    .component('materialsList',{
        template:tem,
        controller:ctrl,
        controllerAs:'materialsListCtrl',
        bindings:{
            type1:'<',
            type2:'<'
        }
    })
    .name;