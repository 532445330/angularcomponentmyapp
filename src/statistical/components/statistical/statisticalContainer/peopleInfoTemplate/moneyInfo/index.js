/**
 * Created by wanglianxin on 2018/11/13.
 */
let tem = require('./moneyInfo.html');
let dealStatis = require('./dealStatis');
let ioStatis = require('./ioStatis');
require('./moneyInfo.scss');
class ctrl {
    constructor($state,$stateParams) {
        this.state= $state;
        console.log($stateParams);
        let filter = $stateParams.moneyInfo;
        this.headOption = {
            title: '交易统计',
            monthSel: [
                {
                    name: '近三个月',
                    value: '3'
                },
                {
                    name: '近六个月',
                    value: '6'
                },
                {
                    name: '近一年',
                    value: '12'
                }
            ],
            defaultIndex:null,
            placeholder:'选择时间范围',
            idName : 'dealStatisDate',
            dateType: 'date',
            stateUrl:'statistical.moneyInfo.1',
            optionName:'dealStatisOption'
        };
        this.ioStatisOption={
            title: '收支统计',
            monthSel: [
                {
                    name: '日视图',
                    value: 'date'
                },
                {
                    name: '月视图',
                    value: 'month'
                }
            ],
            defaultIndex:0,
            placeholder:'选择时间范围',
            idName : 'ioStatisDate',
            dateType: 'date',
            stateUrl:'statistical.moneyInfo.1',
            optionName:'ioStatisOption'
        };
        // this.state.go('.ioStatis',{ioStatisOption:{}});
        // this.state.go('.dealStatis',{dealStatisOption:1});
        this.state.go('.1',{dealStatisOption:'1',ioStatisOption:'2'});

    }
}
ctrl.$inject = ['$state','$stateParams'];
module.exports = angular.module('moneyInfo', [dealStatis,ioStatis])
    .component('moneyInfo',{
        template:tem,
        controller:ctrl,
        controllerAs:'moneyInfoCtrl'
    })
    .name;