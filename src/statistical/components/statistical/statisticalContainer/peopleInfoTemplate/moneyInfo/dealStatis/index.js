/**
 * Created by wanglianxin on 2018/11/15.
 */
let tem = require('./dealStatis.html');
require('./dealStatis.scss');
class ctrl {
    constructor($stateParams,$timeout,publicFn) {
        let _this = this;
        this.data = $stateParams.option;
        this.publicFn=publicFn;
        // todo 获取完请求数据在该页面请求 然后操作数据

        this.dataList=[
            {
                name:'交易金额TOP10',
                value:'callNum_'+this.publicFn.radomInt(2),
                xData:['王阿姨','李先生','真功夫','超过8个字隐藏','分开的斐林试剂','张大爷','李大妈','妈妈','小弟','哥哥'],
                yData:[100,90,80,70,60,50,40,30,20,10],
                barColor:'#539dff'
            },{
                name:'交易频次TOP10',
                value:'callTime_'+this.publicFn.radomInt(2),
                xData:['王阿姨','李先生','真功夫','超过8个字隐藏','分开的斐林试剂','张大爷','李大妈','妈妈','小弟','哥哥'],
                yData:[100,90,80,70,60,50,40,30,20,10],
                barColor:'#4abb52'
            }
        ];
        this.color=['#539dff','#66c56c','#f6cc35','#e13b5b','#8c5ee2','#5ec7c5'];
        this.dealPercent=[
            {
                name:'支付宝',
                value:14544
            },
            {
                name:'微信',
                value:10900
            },

            {
                name:'银联支付',
                value:5000
            },
            {
                name:'PayPal',
                value:2000
            },
            {
                name:'蚂蚁花呗',
                value:500
            },
            {
                name:'其他',
                value:30
            },
        ];
        this.dealNum=200;
        this.allNum=0;   //总金额
        this.dealPercent = this.dealPercent.map((obj,index)=>{
            obj.itemStyle={
                color:this.color[index]
            };
            return obj;
        });
        for(let i=0;i<this.dealPercent.length;i++){
            this.allNum+=Number(this.dealPercent[i].value);
        }
        $timeout(()=>{
            let pieChart = echarts.init(document.getElementById('deal-percent-chart'));
            let pieOption = {
                title : {           //标题
                    text: '总交易次数',
                    subtext:this.dealNum,
                    left:'29.5%',       //为了使标题在环形图中间 left要小于图形left 0.5%
                    top:120,
                    textAlign: 'center',    // todo 需要注意 文档里没有该属性
                    textStyle:{             //主标题样式
                        color:'#929292',
                        fontWeight:'normal',
                        fontSize:14,
                        align:'center'
                    },
                    subtextStyle:{       //副标题样式
                        color:'#272727',
                        fontWeight:'normal',
                        fontSize:16,
                        align:'center',
                    },
                    itemGap:14     //主副标题间距

                },
                legend: {           //图例组件
                    type: 'plain',
                    orient: 'vertical',
                    right: '25%',
                    top: 34,
                    icon: 'circle',
                    itemGap:10,
                    itemWidth:8,
                    itemHeight:8,
                    textStyle:{
                        fontSize:14,
                        rich:{
                            a:{
                                color:'#5a5a5a',
                                padding:[0,8]
                            },
                            b:{
                                color:'#929292',
                                padding:[6,8]
                            },
                            c:{
                                color:'#5a5a5a',
                                padding:[6,8],
                                align:'right'
                            }
                        }
                    },
                    data: this.dealPercent,
                    formatter: function (name) {
                        let valueNum =Number(_this.dealPercent.filter(function(item){return item.name==name})[0].value);

                        let value = _this.publicFn.addCommaToThousands(''+valueNum);
                        return '{a|'+name+'}|{b|'+parseInt(((100*valueNum)/_this.allNum))+'%}{c|￥'+value+'}';
                    },

                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name:'支付方式占比',
                    type:'pie',
                    radius:[84,109],
                    label:{
                        show:false
                    },
                    center:['30%',148],
                    itemStyle: { // 饼图中间白边
                        normal: {
                            borderWidth: 2,
                            borderColor: '#ffffff',
                        }
                    },
                    data:this.dealPercent
                }],

            };
            pieChart.setOption(pieOption);
            $(window).on('resize',()=>{
                pieChart.resize();
            })
        },0);




    }
}
ctrl.$inject =['$stateParams','$timeout','publicFn'];
module.exports = angular.module('dealStatis', [])
    .component('dealStatis', {
        template: tem,
        controller: ctrl,
        controllerAs: 'dealStatisCtrl'
    })
    .name;