/**
 * Created by wanglianxin on .
 */
let tem = require('./ioStatis.html');
require('./ioStatis.scss');

class ctrl {
    constructor($stateParams,$timeout,publicFn) {
        this.publicFn = publicFn;
        this.stateParams=$stateParams;
        let _this =this;
        $timeout(()=>{
            let color = ['#8c5ee2','#5ec7c5','red'];
            this.xData=['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月','1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月','1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'];
            this.allIn=[1200,1100,1000,900,800,700,600,500,400,300,200,100,1200,1100,1000,900,800,700,600,500,400,300,200,100,1200,1100,1000,900,800,700,600,500,400,300,200,100];
            this.allOut=[1200,1100,1000,900,800,700,600,500,400,300,200,100,1200,1100,1000,900,800,700,600,500,400,300,200,100,1200,1100,1000,900,800,700,600,500,400,300,200,100];
            this.allCount=[120,110,100,90,80,70,60,50,40,30,20,10,120,110,100,90,80,70,60,50,40,30,20,10,120,110,100,90,80,70,60,50,40,30,20,10];
            this.allInCount=0;
            this.allOutCount=0;
            this.allCountCount=0;
            for(let i=0;i<this.xData.length;i++){
                this.allInCount+=Number(this.allIn[i]);
                this.allOutCount+=Number(this.allOut[i]);
                this.allCountCount+=Number(this.allCount[i]);
            }
            this.allInCount=publicFn.addCommaToThousands(''+this.allInCount);
            this.allOutCount=publicFn.addCommaToThousands(''+this.allOutCount);

            this.ioChart=echarts.init(document.getElementById('io-statis-chart'));
            this.options = {
                color: color,
                legend:{
                    type: 'plain',
                    orient: 'vertical',
                    left: '83%',
                    top: 40,
                    icon: 'rect',
                    itemGap:47,
                    itemWidth:3,
                    itemHeight:14,
                    selectedMode:false,
                    textStyle:{
                        padding:[0,0,-40,8],    // todo 记录这里自定义legent需要注意

                        rich:{
                            a:{
                                color:'#929292',
                                fontSize:14,
                            },
                            b:{
                                color:'#2e2e2e',
                                lineHeight:40,
                                fontSize:18
                            }
                        }
                    },
                    formatter:function(value){
                        let num ;
                        if(value=='总支出'){
                            num=_this.allOutCount;
                        }
                        else if(value=='总收入'){
                            num=_this.allInCount;
                        }
                        else{
                            num=_this.allCountCount;
                        }
                        let arr = [`{a|${value}}`,`{b|${num}}`];
                        return arr.join('\n');
                    }
                },
                grid: {         //整个网格图块样式
                    left: '3%',
                    right: '20%',
                    height:160,
                    top:70,
                    containLabel: true
                },
                tooltip: {
                    show: "true",
                    trigger: 'item'
                },
                xAxis : [
                    {
                        name:'',
                        type : 'category',
                        data : this.xData,
                        axisTick: {
                            alignWithLabel: true,
                            lineStyle:{
                                width:0
                            }
                        },
                        axisLine:{
                            lineStyle:{
                                color:'#a0a0a0'
                            }
                        },
                        axisLabel:{
                            color:'#a0a0a0',
                            formatter: function(value){
                                return `{a|${value}}`;
                            },
                            rich:{
                                a:{
                                    lineHeight:26
                                }
                            }
                        }
                    }
                ],
                yAxis : [
                    {
                        name:'金额(元)',
                        type : 'value',
                        axisLabel:{
                            color:'#a0a0a0',
                        },
                        axisLine:{
                            show:false
                        },
                        axisTick: {
                            lineStyle:{
                                width:0
                            }
                        },
                        splitLine: {
                            lineStyle: {
                                type: 'dashed'
                            }
                        },
                        nameGap:20
                    },
                    {
                        type:'value',
                        name:'交易次数(次)',
                        axisLine:{
                            show:false
                        },
                        axisLabel:{
                            color:'#a0a0a0',
                        },
                        axisTick: {
                            lineStyle:{
                                width:0
                            }
                        },
                        splitLine: {
                            lineStyle: {
                                type: 'dashed'
                            }
                        },
                        nameGap:20
                    }
                ],
                dataZoom: [
                    {
                        type:"slider",
                        start:0,
                        height:38,
                        // color:'#e6e6e6',
                        endValue:11,
                        zoomLock:true,
                        // backgroundColor:'#fff',
                        fillerColor:'rgba(206,223,243,.5)',
                        // borderColor:'#fff',
                        // handleIcon:'null',
                        //showDataShadow:false,         //不显示数据阴影
                        //showDetail:false,   //拖拽时不显示数据
                        top:211,
                        handleStyle:{
                            color:'rgba(119,175,249,.7)'
                        },
                        textStyle:{
                            color:'#a0a0a0',
                            fontSize:14
                        }
                    },
                    {
                        type: 'inside',
                        start:0,
                        endValue:9,
                        zoomLock:true
                    },
                ],
                series : [
                    {
                        name:'总收入',
                        type:'bar',
                        barWidth: '16',
                        large:true,
                        data:this.allIn
                    },
                    {
                        name:'总支出',
                        type:'bar',
                        barWidth: '16',
                        large:true,
                        data:this.allOut
                    },
                    {
                        name:'总交易次数',
                        type:'line',
                        yAxisIndex: 1,
                        data:this.allCount
                    },
                ]
            };
            this.ioChart.setOption(this.options);
            $(window).on('resize',()=>{
                this.ioChart.resize();
            })
        },0)

    }
}
ctrl.$inject=['$stateParams','$timeout','publicFn'];
module.exports = angular.module('ioStatis', [])
    .component('ioStatis', {
        template: tem,
        controller: ctrl,
        controllerAs: 'ioStatisCtrl'
    }).name;