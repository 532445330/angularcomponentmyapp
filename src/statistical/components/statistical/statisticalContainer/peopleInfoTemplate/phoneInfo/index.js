/**
 * Created by wanglianxin on 2018/11/10.
 */
let tem = require('./phoneInfo.html');
require('./style.scss');
class ctrl {
    constructor($stateParams,http) {
        let _this = this;
        this.tableClassName='phone-info-list';
        this.showThead=false;
        this.tableHeadList='';
        this.tableBodyList=[
            ['设备名称','iphone5'],
            ['设备类型','iphone'],
            ['产品类型','iphone5.3'],
            ['系统版本','iphone10.3.3'],
            ['设备标识','92bc8287gf89s0d9f8g7h89c0d9s87f9f0gdh0fg'],
            ['WIFI地址','0c:30:21:6b:d4:94'],
            ['蓝牙地址','0c:30:6c:d3:95'],
            ['手机串号（IMEI）','874673564758697']
        ];
        this.tableWidth='100%';
        this.tableBg='#fff';
        this.trLineHeight='54px';
        this.trBorderB='none';
        this.isInterval=true;
        this.theadBg='';
        this.trEvenBg='#f9f9f9';
        this.trOddBg='#fff';
        this.tdPadding='0 30px';
        this.tableBodyList=[
            ['设备名称'],
            ['设备类型'],
            ['产品类型'],
            ['系统版本'],
            ['设备标识'],
            ['WiFi地址'],
            ['蓝牙地址'],
            ['手机串号(IMEI)'],
            ['采集设备版本']
        ];

        http.post(
            "http://21.26.96.109:8080/jwcloud/html/getPhoneInfo",
            {dogId:369166805,dataSourceName:'美亚报表',uuid:'美亚仿真_d9eb7d02-3d5f-4ec7-a330-fa1e72bf2d7f'},
            (data)=>{
                if(data.status===0){
                    let dataList = data.dataList;
                    console.log(dataList);
                    for(let i=0;i<_this.tableBodyList.length;i++){
                        let key = _this.tableBodyList[i][0];
                        _this.tableBodyList[i][0][1]=dataList[key];
                    }
                }
            },(err)=>{
            console.log(err);
            }
        );
    }

}
ctrl.$inject=['$stateParams','http'];
module.exports = angular.module('phoneInfo', [])
    .component('phoneInfo',{
        template:tem,
        controller:ctrl,
        controllerAs:'phoneInfoCtrl'
    })
    .name;