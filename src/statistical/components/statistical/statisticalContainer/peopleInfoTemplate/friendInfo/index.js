/**
 * Created by wanglianxin on 2018/11/10.
 */
let tem = require('./friendInfo.html');
let friendStatis = require('./friendStatis/index');
require('./friendInfo.scss');
class ctrl {
    constructor($state,http) {
        let _this =this;
        this.state=$state;
        this.relative=[
            {
                'app':'微信',
                'name':'王阿姨',
                'account':'1235f46879'
            },
            {
                'app':'qq',
                'name':'鸟爸爸',
                'account':'54587821'
            },
            {
                'app':'微信',
                'name':'王阿姨',
                'account':'123546879'
            },
            {
                'app':'qq',
                'name':'鸟爸爸',
                'account':'54587821'
            },
            {
                'app':'微信',
                'name':'王阿姨',
                'account':'123546879'
            }
        ];
        this.relationship=[
            {
                'app':'微信',
                'name':'王阿姨',
                'account':'123546879'
            },
            {
                'app':'qq',
                'name':'鸟爸爸',
                'account':'54587821'
            },
            {
                'app':'微信',
                'name':'王阿姨',
                'account':'123546879'
            },
            {
                'app':'qq',
                'name':'鸟爸爸',
                'account':'54587821'
            },
            {
                'app':'微信',
                'name':'王阿姨',
                'account':'123546879'
            }
        ];
        this.special=[
            {
                'app':'微信',
                'name':'王阿姨',
                'account':'123546879'
            },
            {
                'app':'qq',
                'name':'鸟爸爸',
                'account':'54587821'
            }
        ];
        this.headOption = {
            title: '数据统计',
            monthSel: [
                {
                    name: '近三个月',
                    value: '3'
                },
                {
                    name: '近六个月',
                    value: '6'
                },
                {
                    name: '近一年',
                    value: '12'
                }
            ],
            placeholder:'选择时间范围',
            idName : 'moneyInfoSetDate',
            dateType: 'date',
            stateUrl:'statistical.friendInfo.friendStatis',
            optionName:'friendOption'
        };
        http.post(
            "http://21.26.96.109:8080/jwcloud/html/getSpecialFriendsList",
            {dogId:369166805,dataSourceName:'美亚报表',uuid:'美亚仿真_d9eb7d02-3d5f-4ec7-a330-fa1e72bf2d7f'},
            (data)=>{
                if(data.status===0){
                    _this.relative=data.data.relative;
                    _this.relationship=data.data.relationship;
                    _this.special = data.data.special;
                }
            },(err)=>{
                console.log(err);
            }
        );


        this.startTime='';
        this.endTime='';
        this.setMonthIndex=null;

        this.state.go('.friendStatis',{friendOption:1});

    }
    setDate(value,index){
        if(this.setMonthIndex==index)return;
        this.setMonthIndex=index;
        let time = this.getCurTime();
        this.startTime=time.time;
        let targetTime=this.getTargetTime(time,value);
        this.endTime=targetTime;
        let data={
            start:this.startTime,
            end:this.endTime
        };
        // TODO 这里需要记录 二次点击加载需要写全路径 解决$state.go不执行问题
        this.state.go('statistical.friendInfo.friendStatis',{option:JSON.stringify(data)},{reload:'statistical.friendInfo.friendStatis'});
    };
    getCurTime(){
        let date = new Date();
        let [y,mo,d]=[date.getFullYear(),this.publicFn.toDou(date.getMonth()+1),this.publicFn.toDou(date.getDate())];
        return {year:y,month:mo,date:d,time:`${y}-${mo}-${d}`};
    }
    getTargetTime(time,value){
        let [y,mo,d]=[time.year,time.month,time.date];
        let month=mo+Number(value);
        if(month>12){
            y++;
            month=month-12;
        }
        return `${y}-${month}-${d}`;
    }

}
ctrl.$inject = ['$state','http'];
module.exports = angular.module('friendInfo', [friendStatis])
    .component('friendInfo',{
        template:tem,
        controller:ctrl,
        controllerAs:'friendInfoCtrl'
    })
    .name;