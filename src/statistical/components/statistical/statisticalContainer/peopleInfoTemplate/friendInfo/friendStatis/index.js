/**
 * Created by wanglianxin on 2018/11/13.
 */
let tem = require('./friendStatis.html');
require('./friendStatis.scss');
class ctrl {
    constructor($stateParams,http) {
        this.data = $stateParams.friendOption;
        // todo 获取完请求数据在该页面请求 然后操作数据
        function radomInt(n){
            return parseInt(Math.random()*Math.pow(10,n));
        }
        let start = '';
        let end = '';
        let timeDate=$stateParams.friendOption?anuglar.from($stateParams.friendOption):'';
        if($stateParams.friendOption){
            let timeDate=anuglar.from($stateParams.friendOption);
            start = timeDate.start;
            end = timeDate.end;
        }
        http.post(
            'http://21.26.96.109:8080/'
        )
        this.dataList=[
            {
                name:'通话次数TOP10',
                value:'callNum_'+radomInt(2),
                xData:['王阿姨','李先生','真功夫','超过8个字隐藏','分开的斐林试剂','张大爷','李大妈','妈妈','小弟','哥哥'],
                yData:[100,90,80,70,60,50,40,30,20,10],
                barColor:'#539dff'
            },{
                name:'通话时长TOP10',
                value:'callTime_'+radomInt(2),
                xData:['王阿姨','李先生','真功夫','超过8个字隐藏','分开的斐林试剂','张大爷','李大妈','妈妈','小弟','哥哥'],
                yData:[100,90,80,70,60,50,40,30,20,10],
                barColor:'#4abb52'
            },{
                name:'短信联系TOP10',
                value:'message_'+radomInt(2),
                xData:['王阿姨','李先生','真功夫','超过8个字隐藏','分开的斐林试剂','张大爷','李大妈','妈妈','小弟','哥哥'],
                yData:[100,90,80,70,60,50,40,30,20,10],
                barColor:'#f6cc35'
            },{
                name:'微信联系TOP10',
                value:'wechat_'+radomInt(2),
                xData:['王阿姨','李先生','真功夫','超过8个字隐藏','分开的斐林试剂','张大爷','李大妈','妈妈','小弟','哥哥'],
                yData:[100,90,80,70,60,50,40,30,20,10],
                barColor:'#445081'
            },{
                name:'QQ/TIM联系TOP10',
                value:'qq_'+radomInt(2),
                xData:['王阿姨','李先生','真功夫','超过8个字隐藏','分开的斐林试剂','张大爷','李大妈','妈妈','小弟','哥哥'],
                yData:[100,90,80,70,60,50,40,30,20,10],
                barColor:'#4abb52'
            }
        ]
    }
}
ctrl.$inject=['$stateParams','http'];
module.exports = angular.module('friendStatis', [])
    .component('friendStatis', {
        template: tem,
        controller: ctrl,
        controllerAs: 'friendStatisCtrl'
    })
    .name;