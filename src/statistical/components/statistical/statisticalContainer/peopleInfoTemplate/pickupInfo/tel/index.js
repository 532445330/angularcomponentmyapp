/**
 * Created by wanglianxin on 2018/11/19.
 */
let tem = require('./tel.html');
// require('');
class ctrl {
    constructor() {
        this.data= {
            "status":0,
            allCount:'一共数据条数',
            page:'当前页',
            allPage:'总页数',
            list:[
                    ['姓名','对方号码','归属地','卡的类型','删除'],    //引号里是数据，title不用返回
                    ['']
                ]
        };
        this.dataList=[
            ['李玉刚','1354458796','辽宁省大连市','辽宁移动GSM卡','否'],
            ['李玉刚','1354458796','辽宁省大连市','辽宁移动GSM卡','否'],
            ['李玉刚','1354458796','辽宁省大连市','辽宁移动GSM卡','否'],
            ['李玉刚','1354458796','辽宁省大连市','辽宁移动GSM卡','否'],
            ['李玉刚','1354458796','辽宁省大连市','辽宁移动GSM卡','否'],
            ['李玉刚','1354458796','辽宁省大连市','辽宁移动GSM卡','否']

        ];
        this.tableClassName='tel-list';
        this.showThead=true;
        this.tableHeadList=['姓名','对方号码','归属地','卡的类型','删除'];
        this.tableBodyList=this.dataList;
        this.tableWidth='100%';
        this.tableBg='';
        this.trLineHeight='54px';
        this.trBorderB='1px solid #e5e5e5';
        this.isInterval=0;
        this.theadBg='#f9f9f9';
        this.trEvenBg='';
        this.trOddBg='';
        this.tdPadding='0 30px';
        this.trPadding='0 30px';

        this.args ={
            pageCount:40,//总页码,默认10
            current:1,//当前页码,默认1
            countPer:10, //每页多少条
            PerCountList:[10,20,40,60],        //每页条数选择列表
            allCount:295,    //总数据条数
            showPrev:true,//是否显示上一页按钮
            showNext:true,//是否显示下一页按钮
            showIconBtn:false,//显示箭头上下页
            showFirst:false,  //是否显示首页
            showLast:false,  //是否显示末页
            showTurn:true,//是否显示跳转,默认显示
            showSumNum:true,//是否显示总页码
            showNear:1,//显示当前页码前多少页和后多少页，默认2
            pageSwap:true,//是否同意调剂,默认是。调剂会最大数量显示页码。例如当前页码之前无页码可以显示。则会增加之后的页码。
            align:'left',//对齐方式。默认右对齐.可选：left,right
            frame:''
        };
        this.style={
            fontSize:12,//字体大小
            height:32,//页码总高度，默认20px
            pagesMargin:8,//每个页码或按钮之间的间隔
            paddL:0,//左边留白
            paddR:0,//右边留白
            borderColor:"#d4d4d4",//边线颜色
            color:"#5a5a5a",
            currentBorderColor:"#348bff",//当前页码边线
            currentColor:"#fff",//当前页码的字体颜色
            currentBgColor:"#348bff",//当前页码的背景
            disableColor:"#bab9be",//不可点击按钮的字体颜色
            disableBackColor:"#fff",//不可点击按钮的背景色
            disableborderColor:"#dcdcdc",
            prevNextWidth:32,//上页下页的宽度
            pagecountWidth:32,//共计多少页的宽度
            btnPadding:11 //按钮左右padding
        };
    }
}
module.exports = angular.module('tel', [])
    .component('tel', {
        template: tem,
        controller: ctrl,
        controllerAs: 'telCtrl'
    })
    .name;