/**
 * Created by wanglianxin on 2018/11/13.
 */
let tem = require('./pickupInfo.html');
let tel = require('./tel');


require('./pickupInfo.scss');
class ctrl {
    constructor($state) {
        this.currentIndex=0;
        this.state = $state;
        this.navList = [
            {
                name: '通讯录',
                value:'tel',
                num: 1985
            },
            {
                name: '短信',
                value:'msg',
                num: 2102
            },
            {
                name: '通话记录',
                value:'call',
                num: 2213
            },
            {
                name: '照片',
                value:'pic',
                num: 4591
            },
            {
                name: 'QQ消息',
                value:'qq',
                num: 10103
            },
            {
                name: '微信消息',
                value:'wechat',
                num: 25486
            },
            {
                name: '网页内容',
                value:'web',
                num: 555555
            },
            {
                name: '定位信息',
                value:'pos',
                num: 56
            },
            {
                name: '应用信息',
                value:'app',
                num: 103
            },
            {
                name: '虚拟身份',
                value:'vir',
                num: 22
            },
        ];
        console.log(`.${this.navList[this.currentIndex].value}`);
        this.state.go(`.${this.navList[this.currentIndex].value}`);
    }
}
ctrl.$inject = ['$state'];
module.exports = angular.module('pickupInfo', [tel])
    .component('pickupInfo', {
        template: tem,
        controller: ctrl,
        controllerAs: 'pickupInfoCtrl'
    })
    .name;