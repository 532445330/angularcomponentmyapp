/**
 * Created by wanglianxin on 2018/11/13.
 */
let tem = require('./attentionInfo.html');
require('./style.scss');
class ctrl {
    constructor() {

    }
}
module.exports = angular.module('attentionInfo', [])
    .component('attentionInfo', {
        template: tem,
        controller: ctrl,
        controllerAs: 'attentionInfoCtrl'
    })
    .name;