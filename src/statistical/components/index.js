/**
 * Created by wanglianxin on 2018/11/8.
 */
let headerC = require('./head');
let statistical = require('./statistical');

module.exports = angular.module('components',[
    headerC,
    statistical
]).name;