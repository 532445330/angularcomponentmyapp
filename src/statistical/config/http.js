/**
 * Created by wanglianxin on 2018/11/21.
 */
class http {
    constructor($http) {
        this.http = $http;
    }
    get(url,data,resolve,reject){
        let get = {url:url,method:'GET',data:data};
        return this.http(get).then((data)=>{
            console.log('success');
            resolve(data.data);
        },(err)=>{
            reject(err);
        })
    }
    post(url,data,resolve,reject){
        let get = {url:url,method:'POST',data:data};
        return this.http(get).then((data)=>{
            console.log('success');
            resolve(data.data);
        },(err)=>{
            reject(err);
        })
    }
}

http.$inject = ['$http'];
// module.exports = angular.module('http', [])
//     .service('http',http)
//     .name;
module.exports=http;