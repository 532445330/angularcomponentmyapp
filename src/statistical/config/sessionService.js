/**
 * Created by wanglianxin on 2018/11/17.
 */
class sessionService{
    constructor(){

    }
    setStorage(key,value){
        let json;
        if(value=='undefined'){
            json=null;
        }else{
            angular.toJson(value);
        }
        return sessionStorage.setItem(key,json);
    }
    getStorage(key){
        return angular.fromJson(sessionStorage.getItem(key))
    }
    clear(){
        let result=[];
        for(let key in sessionStorage){
            result.push(this.setStorage(key,null));
        }
        return result;
    }
    stateHistory(value){     //value如果是false  返回stateHistory的值，如果有值 则存上
        return this.accessor('stateHistory',value);
    }
    accessor(name,value){
        if(value==null){
            return this.getStorage(name);
        }
        return this.setStorage(name,value);
    }
}

// module.exports = angular.module('app.services',[])
// .service('sessionServices',sessionService);
module.exports=sessionService;