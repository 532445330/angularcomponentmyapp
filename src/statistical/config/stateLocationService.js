/**
 * Created by wanglianxin on 2018/11/17.
 */
class service {
    constructor($location,$state,stateHistoryService) {
        this.location = $location;
        this.state = $state;
        this.stateHistoryService = stateHistoryService;
        this.preventCall=[];
    }
    locationChange(){
        // console.log('locationChange');
        let entry;
        if (this.preventCall.pop('locationChange') != null) {
            entry = this.stateHistoryService.get(this.location.url());
            if (entry == null) {
                return;
            }
            this.preventCall.push('stateChange');
            return this.state.go(entry.name, entry.params, {
                location: false
            });
        }
    }
    stateChange(){
        // console.log('stateChange');
        let entry, url;
        if (!!this.preventCall.pop('stateChange')) {
            entry = {
                name: this.state.current.name,
                params: this.state.params
            };
            url = "/" + this.state.params.subscriptionUrl + "/" + (Math.guid().substr(0,8));
            this.stateHistoryService.set(url, entry);
            this.preventCall.push('locationChange');
            return this.location.url(url);
        }
    }
}
service.$inject = ['$location','$state','stateHistoryService'];
// module.exports = angular.module('app.services', [])
//     .service('stateLocationService',service);
module.exports=service;