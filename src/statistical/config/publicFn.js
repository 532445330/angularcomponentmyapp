/**
 * Created by wanglianxin on 2018/11/14.
 */
class publicFn{
    constructor(){

    }
    toDou(n){
        return n<10?'0'+n:n;
    }
    findArrSame(arr,n,key){
        for(let i=0;i<arr.length;i++){
            if(key){
                if(arr[i][key]==n){
                    return true;
                }
            }else{
                if(arr[i]==n){
                    return true;
                }
            }
        }
        return false;
    }
    radomInt(n){
        return parseInt(Math.random()*Math.pow(10,n));
    }
    addCommaToThousands(n){
        return n.replace(/(\d)(?=(\d{3})+$)/g, "$1,")
    }
}

module.exports=publicFn;
