/**
 * Created by wanglianxin on 2018/11/13.
 */

class service{
    constructor(){
        this.layDate={};
        this.currentId={};
    }
    setDate(name,data){
        this.layDate[name]=data;
    }
    getDate(name){
        if(!!name){
            return this.layDate[name]
        }else{
            return this.layDate
        }
    }
    setCurrentId(data){
        this.currentId=data;
    }
    getCurrentId(){
        return this.currentId;
    }
}

module.exports =service;