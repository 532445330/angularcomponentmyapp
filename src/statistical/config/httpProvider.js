/**
 * Created by wanglianxin on 2018/11/21.
 */
;
class httpProvider {
    constructor($httpProvider) {
        $httpProvider.defaults.headers.post={ 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' };
        $httpProvider.defaults.transformRequest = function(data){
            if(typeof(data)=='object'){
                return $.param(data,true);
            }else{
                return data;
            }
        };
        $httpProvider.defaults.withCredentials=true;
    }
}
httpProvider.$inject=['$httpProvider'];
module.exports=httpProvider;