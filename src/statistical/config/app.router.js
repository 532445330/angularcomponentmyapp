/**
 * Created by wanglianxin on 2018/11/6.
 */
class routing{
    constructor($stateProvider, $locationProvider, $urlRouterProvider){
        [this.stateProvider,this.locationProvider,this.urlRouterProvider]=[$stateProvider, $locationProvider, $urlRouterProvider];
        // this.locationProvider.html5Mode(true); //去除#号 但是热更新时 不能刷新页面
        $urlRouterProvider.when('', '/statistical.html');
        // this.urlRouterProvider.otherwise('/analyze');
        this.urlRouterProvider.otherwise('/statistical');
        this.stateProvider.state('statistical', {
            url: '/statistical',
            template: '<statistical style="height: 100%"></statistical>'
        });

        // this.stateProvider.state('statistical.peopleInfo',{
        //     url:'/peopleInfo/:sub',
        //     template:'<people-info style="height:100%;">></people-info>'
        // });
        this.stateProvider.state('statistical.baseInfo',{
            url:'/baseInfo/:sourceId',
            params:{

            },
            template:'<base-info style="height:100%;"></base-info>'
        });
        this.stateProvider.state('statistical.phoneInfo',{
            url:'/phoneInfo/:sourceId',
            template:'<phone-info style="height:100%;"></phone-info>',
            resolve:{
                getPhoneInfo:($stateParams)=>{
                    // console.log($stateParams);
                }
            }
        });
        this.stateProvider.state('statistical.friendInfo',{
            url:'/friendInfo/:sourceId',
            template:'<friend-info style="height:100%;"></friend-info>',
        });
        this.stateProvider.state('statistical.friendInfo.friendStatis',{
            url:'/friendStatis/:friendOption',
            template:'<friend-statis style="height:100%;"></friend-statis>'
        });
        this.stateProvider.state('statistical.moneyInfo',{
            url:'/moneyInfo/:sourceId',
            template:'<money-info style="height:100%;"></money-info>'
        });
        // TODO 特别注意这里 多命名ui-view 处理方式
        this.stateProvider.state('statistical.moneyInfo.1',{
            url:'/1/:ioStatisOption/:dealStatisOption',
            views:{
                "dealStatis":{
                    template:'<deal-statis style="height:100%;"></deal-statis>'
                },
                "ioStatis":{
                    template:'<io-statis style="height:100%;"></io-statis>'
                }
            },

        });
        // this.stateProvider.state('statistical.peopleInfo.moneyInfo.dealStatis',{
        //     // url:'/dealStatis/:dealStatisOption',
        //     views:{
        //         'dealStatis':'<deal-statis style="height:100%;"></deal-statis>'
        //     }
        // });
        // this.stateProvider.state('statistical.peopleInfo.moneyInfo.ioStatis',{
        //     // url:'/ioStatis/:ioStatisOption',
        //     views:{
        //         'ioStatis':'<io-statis style="height:100%;"></io-statis>'
        //     }
        // });
        this.stateProvider.state('statistical.pickupInfo',{
            url:'/pickupInfo/:sourceId',
            template:'<pickup-info style="height:100%;"></pickup-info>'
        });
        // this.stateProvider.state('tel',{
        //     url:'/tel',
        //     parent:'statistical.peopleInfo.pickupInfo',
        //     template:'<tel style="height:100%;"></tel>'
        // });
        this.stateProvider.state('statistical.pickupInfo.tel',{
            url:'/tel',
            // parent:'statistical.peopleInfo.pickupInfo',
            template:'<tel style="height:100%;"></tel>'
        });
        this.stateProvider.state('statistical.attentionInfo',{
            url:'/attentionInfo:sourceId',
            template:'<attention-info style="height:100%;"></attention-info>'
        });

    }
}
routing.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider'];
module.exports = routing;