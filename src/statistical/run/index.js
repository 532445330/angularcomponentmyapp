/**
 * Created by wanglianxin on 2018/11/17.
 */

class run {
    constructor($rootScope,stateLocationService,$transitions) {
        let _this = this;
        this.stateLocationService=stateLocationService;
        $transitions.onSuccess( {}, function(trans) {
            // console.log(trans);
            // console.log('$stateChangeSuccess');
            _this.stateLocationService.stateChange();
        });
        $rootScope.$on('$locationChangeSuccess',()=>{
            // console.log('$locationChangeSuccess');
            this.stateLocationService.locationChange();
        })
    }
}
run.$inject=['$rootScope',"stateLocationService","$transitions"];
module.exports = run;