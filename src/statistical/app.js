/**
 * Created by wanglianxin on 2018/11/8.
 */
let angular = require('angular');
// todo 这里注意 angular-cookies版本和angularjs版本一致对应
let ngCookies = require('angular-cookies');
let uiRouter = require('angular-ui-router').default;
var components = require('./components');
var commonComponents = require('../commonComponents');
var appRouter = require('./config/app.router');
// let appProvider = require('./config/provider');
let publicFn = require('./config/publicFn');
let service = require('./config/service');

let sessionService = require('./config/sessionService');
let stateHistoryService = require('./config/stateHistoryService');
let stateLocationService = require('./config/stateLocationService');

let http = require('./config/http');
let run = require('./run');
let httpProvider = require('./config/httpProvider');

require('../asset/css/reset.css');
require('../asset/css/style.css');
require('./app.scss');

let appComponent = {
    restrict: 'E',
    template: require('./app.html'),
    controller: function () {

    },
    controllerAs: 'app'
};

module.exports = angular.module('app',[uiRouter,ngCookies,components,commonComponents])
    .config(httpProvider)
    .config(appRouter)
    .service('service',service)
    .factory('publicFn',publicFn)
    .component('app', appComponent)
    .service('sessionService',sessionService)
    .service('stateHistoryService',stateHistoryService)
    .service('stateLocationService',stateLocationService)
    .service('http',http)
    .run(run)
    .name;
