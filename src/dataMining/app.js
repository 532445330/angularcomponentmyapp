var angular = require('angular');
var uiRouter = require('angular-ui-router').default;
var components = require('./components');
var commonComponents = require('../commonComponents');
var appRouter = require('./config/app.router');

require('../asset/css/reset.css');
require('../asset/css/style.css');

let appComponent = {
    restrict: 'E',
    template: require('./app.html'),
    controller: function () {

    },
    controllerAs: 'appC'
};

module.exports = angular.module('app',[uiRouter,components,commonComponents])
    .config(appRouter)
    .component('appC', appComponent)
    .name;
