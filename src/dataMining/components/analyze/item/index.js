/**
 * Created by wanglianxin on 2018/11/6.
 */
let tem = require('./item.html');
require('./item.scss');

class ctrl{
    constructor(){
        this.name='lianxin';
        let _this = this;
    }
}

module.exports = angular.module('navItem',[])
    .component('navItem',{
        restrict: 'E',
        template:tem,
        controller:ctrl,
        controllerAs:'ctrl',    //如果不起名字则用$ctrl
        bindings:{
            data:'<'
        }
    }).name;