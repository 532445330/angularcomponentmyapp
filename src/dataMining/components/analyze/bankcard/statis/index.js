/**
 * Created by wanglianxin on 2018/11/7.
 */
let tem = require('./statis.html');
require('./statis.scss');
class ctrl {
    constructor() {
        let _this = this;
        setTimeout(function () {
            _this.title=_this.data.title;

        });
        console.log(echarts);
        var mychart = echarts.init(document.getElementById('ana-statis'));

        var color = ['#f9ca31','#579dfe'];
        var data=[
            {
                'name':'总支出',
                'value':250
            },
            {
                'name':'总收入',
                'value':750
            }
        ];

        data = data.map(function(obj,index){
            return {
                value:obj.value,
                name:obj.name,
                itemStyle:{
                    color:color[index]
                }
            }
        });
        console.log(data);
        var option = {
            series: [{
                name:'',
                type:'pie',
                radius:[54,68],
                hoverAnimation: false,
                label:{
                    show:true,
                    color:'#545454',
                    formatter:'{d}%'
                },
                labelLine:{
                    normal:{
                        show:true,
                        length:24,
                        length2:28,
                        lineStyle:{
                            color:'#c8c8c8',
                        }
                    }
                },
                data:data
            }],

        };

        mychart.setOption(option);
    }
}
module.exports = angular.module('statis',[])
    .component('statis',{
        restrict: 'E',
        template:tem,
        controller:ctrl,
        controllerAs:'statisCtrl',
        bindings:{
            data:'<'
        }
    }).name;
