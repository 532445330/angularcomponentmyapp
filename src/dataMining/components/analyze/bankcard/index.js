/**
 * Created by wanglianxin on 2018/11/7.
 */
let tem = require('./bankcard.html');
let statis = require('./statis');
let trend = require('./trend');
let selectTime = require('./selectTime');
let setDate = require('./setDate');

class bankcardCtrl {
    constructor(){
        this.statis={
            title:'收支统计'
        };
        this.trend={
            title:'经济走势'
        };
        this.option={
            placeholder:'开始时间'
        }
    }
}
module.exports = angular.module('analyze.bankcard',[statis,trend,selectTime,setDate])
    .component('bankcard',{
        template: tem,
        controller:bankcardCtrl,
        controllerAs:'bankcardCtrl'
    }).name;