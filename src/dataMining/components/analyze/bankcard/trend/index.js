/**
 * Created by wanglianxin on 2018/11/7.
 */
let tem = require('./trend.html');
require('./trend.scss');
class ctrl{
    constructor(){
        let _this = this;
        setTimeout(function(){
            _this.title=_this.data.title;
        },0);
        this.navList=[
            {
                name:'总收入',
                num:'0'
            },
            {
                name:'总支出',
                num:'0'
            },
            {
                name:'总交易次数',
                num:'2000'
            },
            {
                name:'交易对象数',
                num:'10000'
            },
            {
                name:'最大金额交易',
                num:'10000'
            },
            {
                name:'最大交易频次',
                num:'50'
            },
        ];
        let categories =[
            {
                name:'TOP1',
                itemStyle:{
                    color:'#ff7373'
                }
            },
            {
                name:'TOP2',
                itemStyle:{
                    color:'#ffa868'
                }
            },
            {
                name:'TOP4',
                itemStyle:{
                    color:'#ffe168'
                }
            },
            {
                name:'common',
                itemStyle:{
                    color:'#7eb4ff'
                }
            }
        ];
        let xList = ['张444','王53','李v','赵范德萨','张放大萨芬的撒','王','李','赵','张','王','李','赵','张444','王53','李v','赵范德萨','张放大萨芬的撒','王','李','赵','张','王','李','赵'];
        let trendData = [560,543,344,232,200,155,132,100,53,44,43,3,560,543,344,232,200,155,132,100,53,44,43,3];
        let trendChart = echarts.init(document.getElementById('ana-trend-data'));


        let options = {
            grid: {         //整个网格图块样式
                left: '3%',
                right: '10%',
                bottom: '10%',
                containLabel: true
            },

            tooltip: {
                show: "true",
                trigger: 'item',
                formatter: "{b0}:{c0}"
            },
            xAxis : [
                {
                    name:'交易对象',
                    type : 'category',
                    data : xList,
                    axisTick: {
                        alignWithLabel: true,
                        lineStyle:{
                            width:0
                        }
                    },
                    axisLine:{
                        lineStyle:{
                            color:'#a0a0a0'
                        }
                    },
                    axisLabel:{
                        color:'#a0a0a0',
                        formatter: function(value){
                            let ret = "",
                                maxLength = 1,
                                valLength=value.length,
                                rowN = Math.ceil(valLength / maxLength);
                            if(rowN>1){
                                if(rowN<6){
                                    for(let i=0;i<rowN;i++){
                                        let temp = '',
                                            start =i*maxLength,
                                            end=start+maxLength;
                                        temp = value.substring(start,end)+'\n';
                                        ret+=temp;
                                    }
                                }else{
                                    for(let i=0;i<6;i++){
                                        let temp = '',
                                            start =i*maxLength,
                                            end=start+maxLength;
                                        temp = value.substring(start,end)+'\n';
                                        ret+=temp;
                                    }
                                    ret=ret+'...';
                                }

                                return ret;
                            }else{
                                return value;
                            }
                        }
                    }

                }
            ],
            yAxis : [
                {
                    name:'交易次数',
                    type : 'value',
                    axisLabel:{
                        color:'#a0a0a0'
                    },
                    axisLine:{
                        show:false,
                        lineStyle:{
                            color:'#a0a0a0'
                        }
                    },
                    axisTick: {
                        lineStyle:{
                            width:0
                        }
                    },
                }
            ],
            dataZoom: [
                {
                    type:"slider",
                    start:0,
                    height:20,
                    color:'#e6e6e6',
                    endValue:9,
                    zoomLock:true,
                    backgroundColor:'#fff',
                    fillerColor:'#e6e6e6',
                    borderColor:'#fff',
                    handleIcon:'null',
                    showDataShadow:false,         //不显示数据阴影
                    showDetail:false,   //拖拽时不显示数据
                    bottom:5
                },
                {
                    type: 'inside',
                    start:0,
                    endValue:9,
                    zoomLock:true
                },
            ],
            series : [
                {
                    type:'bar',
                    barWidth: '16',
                    // barCategoryGap:'15',
                    large:true,
                    data:trendData,
                    itemStyle:{
                        color:function(item){
                            let index = item.dataIndex;
                            if(item.dataIndex<3){
                                return categories[index].itemStyle.color;
                            }else{
                                return categories[3].itemStyle.color;
                            }
                        },
                        barBorderRadius:[8,8,0,0]
                    }
                }
            ]
        };


        trendChart.setOption(options);
        setTimeout(()=>{
            console.log(this.data);
        },0);


    }

}

module.exports = angular.module('trend',[])
.component('trend',{
    template:tem,
    controller:ctrl,
    controllerAs:'trendCtrl',
    bindings:{
        data:'<'
    }
}).name;