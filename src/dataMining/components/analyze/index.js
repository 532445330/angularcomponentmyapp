/**
 * Created by wanglianxin on 2018/11/6.
 */
let tem = require('./analyze.html');
require('./analyze.scss');
let $ = require('jquery');
let idcard = require('./idcard');
// let phone = require('./analyze/phone');
// let address = require('./analyze/address');
// let ip = require('./analyze/ip');
// let website = require('./website');
let bankcard = require('./bankcard');
// let flight = require('./flight');
// let date = require('./date');
// let consumption = require('./consumption');
// let thesaurus = require('./thesaurus');

class analyzeCtrl{
    constructor(){
        this.name='wanglianixn';
        this.items=[
            {
                name:'身份证',
                class:'idcard',
                totalCount:0
            },
            {
                name:'手机号',
                class:'phone',
                totalCount:0
            },
            {
                name:'地址',
                class:'address',
                totalCount:0
            },
            {
                name:'ip',
                class:'ip',
                totalCount:0
            },
            {
                name:'网址',
                class:'website',
                totalCount:0
            },
            {
                name:'银行卡号',
                class:'bankcard',
                totalCount:0
            },
            {
                name:'航班',
                class:'flight',
                totalCount:0
            },
            {
                name:'时间',
                class:'date',
                totalCount:0
            },
            {
                name:'消费记录',
                class:'consumption',
                totalCount:0
            },
            {
                name:'自定义词库',
                class:'thesaurus',
                totalCount:0
            }
        ];
        this.anaContainerHeight='540px';
    }
}
let item = require('./item');
module.exports = angular.module('app.analyze',[item,idcard,bankcard])
    .component('analyze',{
        template: tem,
        controller: analyzeCtrl,
        controllerAs: 'analyzeCtrl',
    }).name;