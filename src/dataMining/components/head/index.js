/**
 * Created by wanglianxin on 2018/11/6.
 */
let tem = require('./head.html');
require('./head.scss');
class headCtrl{
    constructor(){
        this.titleName='睿海星河';
    }
}
module.exports = angular.module('app.head',[])
    .component('headerC',{
        template: tem,
        controller: headCtrl,
        controllerAs: 'headCtrl'
    }).name;