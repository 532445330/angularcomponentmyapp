/**
 * Created by wanglianxin on 2018/11/6.
 */
class routing{
    constructor($stateProvider, $locationProvider, $urlRouterProvider){
        [this.stateProvider,this.locationProvider,this.urlRouterProvider]=[$stateProvider, $locationProvider, $urlRouterProvider];
        // this.locationProvider.html5Mode(true);
        // this.urlRouterProvider.otherwise('/analyze');
        this.urlRouterProvider.otherwise('/analyze/bankcard/');
        this.stateProvider.state('analyze', {
                url: '/analyze',
                template: '<analyze></analyze>'
            });
        this.stateProvider.state('analyze.idcard',{
            url:'/idcard',
            template: '<idcard></idcard>'
        });
        this.stateProvider.state('analyze.bankcard',{
            url:'/bankcard/:id',
            template: '<bankcard></bankcard>'
        });
    }
}
routing.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider'];
module.exports = routing;