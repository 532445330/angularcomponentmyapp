/**
 * Created by wanglianxin on .
 */
let tem = require('./chatItem.html');
require('./chatItem.scss');

class ctrl {
    constructor() {
        this.data={
            placeholder:'选择时间范围',
            idName : 'moneyInfoSetDate',
            dateType: 'date',
            stateUrl:'statistical.friendInfo.friendStatis',
            optionName:'friendOption'
        };
    }
}

module.exports = angular.module('chatItem', [])
    .component('chatItem', {
        template: tem,
        controller: ctrl,
        controllerAs: 'chatItemCtrl'
    }).name;