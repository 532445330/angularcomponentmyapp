/**
 * Created by wanglianxin on 2018/11/13.
 */
let tem = require('./barCommon.html');
// require('');
class ctrl {
    constructor($timeout) {
        let _this =this;
        $timeout(()=>{
            if(!document.getElementById(this.idName))return;
            let trendChart = echarts.init(document.getElementById(this.idName));
            let options = {
                grid: {         //
                    left: '10%',
                    right: '10%',
                    bottom: '3%',
                    containLabel: true
                },

                tooltip: {
                    show: "true",
                    trigger: 'item',
                    formatter: "{b0}:{c0}"
                },
                xAxis : [
                    {
                        name:'',
                        type : 'category',
                        data : _this.xdata,
                        axisTick: {
                            alignWithLabel: true,
                            lineStyle:{
                                width:0
                            }
                        },
                        axisLine:{
                            lineStyle:{
                                color:'#a0a0a0'
                            }
                        },
                        axisLabel:{
                            color:'#a0a0a0',
                            interval:'auto',
                            rotate:40

                        }
                    }
                ],
                yAxis : [
                    {
                        name: _this.name,
                        nameTextStyle:{
                            color:'#2e2e2e',
                            fontSize:16,
                            fontWeight:'bold',
                            lineHeight:44,
                            padding:[0,16]

                        },
                        type: 'value',
                        axisLabel: {
                            color: 'rgba(0, 0, 0, 0.65)',
                            formatter: function(value){
                                if(value.toString().length>8){
                                    return value.substring(0,8);
                                }else{
                                    return value;
                                }
                            }
                        },
                        axisLine: {                //
                            show: false,
                            lineStyle: {
                                color: '#a0a0a0'
                            }
                        },
                        axisTick: {                 //
                            lineStyle: {
                                width: 0
                            }
                        },
                        splitLine: {
                            lineStyle: {
                                type: 'dashed'
                            }
                        }
                    }
                ],
                dataZoom: [           //
                    {
                        type: 'inside',
                        start:0,
                        endValue:9,
                        zoomLock:true
                    },
                ],
                series : [
                    {
                        type:'bar',
                        barWidth: '16',
                        // barCategoryGap:'15',
                        large:true,
                        data:_this.ydata,
                        itemStyle:{
                            color:_this.barColor
                        }
                    }
                ]
            };
            trendChart.showLoading();
            trendChart.setOption(options);
            trendChart.hideLoading();
            $(window).on('resize',()=>{
                trendChart.resize();
            })
        },500);
    }
}
ctrl.$inject = ['$timeout'];
module.exports = angular.module('barCommon', [])
    .component('barCommon', {
        template: tem,
        controller: ctrl,
        controllerAs: 'barCommonCtrl',
        bindings:{
            idName:'<',
            xdata:'<',
            ydata:'<',
            name:'<',
            barColor:'<'
        }
    })
    .name;