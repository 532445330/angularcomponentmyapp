/**
 * Created by wanglianxin on 2018/11/14.
 */
let tem = require('./dateTem.html');
require('./style.scss');
class ctrl {
    constructor() {

    }
}
module.exports = angular.module('dateTem', [])
    .component('dateTem', {
        template: tem,
        controller: ctrl,
        controllerAs: 'dateTemCtrl',
        bindings:{
            data:'<'
        }
    })
    .name;