/**
 * Created by wanglianxin on 2018/11/19.
 */
let tem = require('./page.html');
// require('jquery');
// let angular = require('angular');
require('./jquery.page');
require('./page.css');
class ctrl {
    constructor($timeout,$window) {
        let _this =this ;
        this.container = $('.page-item');
    //     $('.word-frequency-page').createPage(function(n){
    //
    //     },{
    //         pageCount:$scope.data.formListTotalPageNum,
    //         current:Number($scope.data.formListCurrentPage),
    //         showPrev:true,//是否显示上一页按钮
    //         showNext:true,//是否显示下一页按钮
    //         showIconBtn:true,
    //         showFirst:true,
    //         showLast:true,
    //         align:'left',
    //         pageSwap:false,
    //         showNear:1,
    //         frame:"angular"
    //     },{
    //         "color":"#268aee",
    //         "height":32,
    //         // "pageWidth":36,
    //         "pagesMargin":4,
    //         "borderColor":"#dcdcdc",
    //         "currentColor":"#fff",
    //         "currentBorderColor":"#4098f0",
    //         "currentBgColor":"#4098f0",
    //         "disableColor":'#bfd5ea',
    //         "disableBackColor":"#fff",
    //         "disableborderColor":"#dcdcdc",
    //         "prevNextWidth":42,
    //         "btnPadding":14
    //     });
        $timeout(()=>{
            let width = this.width=parseInt(this.container.css('width'))||parseInt(this.container.parent().css('width'));

            this.PerCountList=[10,20,40,60];
            this.pageNumList=[1,2,3,4,5,6,'...',40];
            this.args = $.extend({
                pageCount:10,//总页码,默认10
                current:1,//当前页码,默认1
                countPer:10, //每页多少条
                PerCountList:[10,20,40],        //每页条数选择列表
                allCount:400,    //总数据条数
                showPrev:true,//是否显示上一页按钮
                showNext:true,//是否显示下一页按钮
                showIconBtn:false,//显示箭头上下页
                showFirst:false,  //是否显示首页
                showLast:false,  //是否显示末页
                showTurn:true,//是否显示跳转,默认显示
                showSumNum:true,//是否显示总页码
                showNear:2,//显示当前页码前多少页和后多少页，默认2
                pageSwap:true,//是否同意调剂,默认是。调剂会最大数量显示页码。例如当前页码之前无页码可以显示。则会增加之后的页码。
                align:'right',//对齐方式。默认右对齐.可选：left,right
                frame:''
            },this.args||{});
            this.style=$.extend({
                "fontSize":14,//字体大小
                "height":20,//页码总高度，默认20px
                "width":width,
                "pageWidth":'',
                "pagesMargin":2,//每个页码或按钮之间的间隔
                "paddL":0,//左边留白
                "paddR":0,//右边留白
                "borderColor":"#428bca",//边线颜色
                "color":"#628FCE",
                "currentBorderColor":"#4678BE",//当前页码边线
                "currentColor":"#ed601b",//当前页码的字体颜色
                "currentBgColor":"#588EDA",//当前页码的背景
                "disableColor":"#bfbfbf",//不可点击按钮的字体颜色
                "disableBackColor":"#f2f2f2",//不可点击按钮的背景色
                "disableborderColor":"#D3D3D3",
                "prevNextWidth":48,//上页下页的宽度
                "pagecountWidth":48,//共计多少页的宽度
                "borderRadius":4,
                "btnPadding":16  //按钮左右padding
            },_this.styleOption||{});

            if(!this.style.pageWidth)this.style.pageWidth=(()=>{
                let sumWidth = this.style.width-(this.style.prevNextWidth+2+this.style.pagesMargin)*(Number(this.args.showPrev)+Number(this.args.showNext))-(this.style.pagecountWidth+this.style.pagesMargin)*Number(this.args.showSumNum)-(this.style.trunWidth+this.style.pagesMargin)*Number(this.args.showTurn);
                let sumLength = this.args.showNear*2+5;
                return parseInt(sumWidth/sumLength)-this.style.pagesMargin;
            });

            this.init();
        });
        // $window.onclick=()=>{
        //     console.log('323');
        //     _this.switchCountper=false;
        // }
    }
    init(){
        this.fillHtml();
    }
    fillHtml(){
        if(this.args.current>this.args.pageCount || this.args.current<1)return;
        this.pageNumList=[];
        this.pageNumList.push(1);

        if(this.args.current>this.args.showNear+2){
            this.pageNumList.push('...');
        }
        let start = this.args.current>this.args.showNear+2?this.args.current-this.args.showNear:2;
        let end = this.args.current+this.args.showNear>=this.args.pageCount?this.args.pageCount-1:this.args.current+this.args.showNear;
        if(this.args.pageSwap){
            let dstart=this.args.current-this.args.showNear-2;
            let dend=this.args.pageCount-1-this.args.current-this.args.showNear;
            if(dstart<1&&dend>1){
                end+=Math.min(dend,Math.abs(dstart-1));
            }else if(dstart>1&&dend<1){
                start-=Math.min(dstart,Math.abs(dend-1));
            }
        }
        for (;start <= end; start++) {
            this.pageNumList.push(start);
        }
        if(this.args.current + 1 + this.args.showNear < this.args.pageCount){
            this.pageNumList.push('...');
        }
        if(this.args.current != this.args.pageCount&&this.args.pageCount!=1) {
            this.pageNumList.push(this.args.pageCount);
        }
       this.setStyle();
    }
    setStyle(){
        let s = this.style;
        let marLR=s.pagesMargin;

        this.childStyle={'width':'auto','float':'left','margin-left':marLR+'px','text-align':'center','border-radius':'4px',"padding":'0 '+s.btnPadding+'px'};
        this.aCommonStyle = {'text-decoration':'none','border':'1px solid '+s.borderColor,'border-radius':'4px',"padding":'0 '+s.btnPadding+'px','color':s.color};
        this.aFirstPage={'width':'46px','height':s.height-2+'px','line-height':s.height-2+'px','padding':0};//首页
        this.aLastPage={'width':'46px','height':s.height-2+'px','line-height':s.height-2+'px','padding':0}; //尾页
        this.aPrevPage={'width':s.prevNextWidth+'px','font-size':s.fontSize+'px','height':s.height-2+'px','line-height':s.height-2+'px',"padding":0};  //上一页
        this.aNextPage = {'width':s.prevNextWidth+'px','font-size':s.fontSize+'px','height':s.height-2+'px','line-height':s.height-2+'px',"padding":0}; //下一页
        this.aTurndown={'margin-left':'18px','display':'inline-block','height':s.height-2+'px','line-height':s.height-2+'px','color':s.color};      //跳转确定按钮
        this.aCurrent = {'display':'inline-block','height':s.height+'px','line-height':s.height+'px','color':s.currentColor,'vertical-align':'middle','background':s.currentBgColor,'border':s.currentBorderColor};   //当前按钮
        this.aDisabled = {'display':'inline-block','height':s.height-2+'px','line-height':s.height-2+'px','color':s.disableColor,'background':s.disableBackColor,'border':'1px solid '+s.disableborderColor,'vertical-align':'middle','font-size':s.fontSize+'px','width':s.prevNextWidth+'px',"padding":0};
        this.aDisabledF = {'display':'inline-block','height':s.height-2+'px','line-height':s.height-2+'px','color':s.disableColor,'background':s.disableBackColor,'border':'1px solid '+s.disableborderColor,'vertical-align':'middle','font-size':s.fontSize+'px','width':'46px','padding':0};
        this.aPagecount = {'width':s.pagecountWidth+'px','font-size':s.fontSize+'px','color':'#999','height':s.height+'px','line-height':s.height+'px'};

        this.spanCountYe={'color':'#4C4C4C','font-size':s.fontSize+'px','width':s.trunWidth+'px'};  //跳转部分整体样式
        this.inputStyle={'outline':'none','border':'1px solid #ddd','height':s.height-2+'px','line-height':s.height-2+'px','width':'24px','text-align':'center','padding':'0 8px',margin: '0 5px','border-radius':"4px"}; //页码输入框样式
        this.tcdNumberStyle={'width':s.pageWidth-2+'px','height':s.height-2+'px','line-height':s.height-2+'px'}; //中间页码样式
        this.hidingStyle = {'width':s.pageWidth+'px','height':s.height+'px','line-height':'36px',"font-weight":'700','color':s.color,'margin-top':'-2px'};  //省略号样式

        this.countPerList={'width':'90%','bottom':s.height+'px','color':s.color,'line-height':s.height-2+'px'};
    }
    mergeStyle(...args){
        let arr = args;
        let targetJson={};
        for(let i=0;i<arr.length;i++) {
            targetJson =$.extend(targetJson,arr[i]);
        }
        return targetJson;
    }
    firstPage(){
        this.args.current=1;
        this.fillHtml();
    }
    prevPage(){
        if(this.args.current>1){
            this.args.current--;
            this.fillHtml();
        }
    }
    loadCurrent($event,num){
        this.args.current=num;
        this.fillHtml();
    }
    nextPage(){
        if(this.args.current<this.args.pageCount){
            this.args.current++;
            this.fillHtml();
        }
    }
    lastPage(){
        this.args.current=this.args.pageCount;
        this.fillHtml();
    }
    skipPage(num){
        var reg=/\d/g;
        if(!reg.test(num) || num> this.args.pageCount|| num<1 || !num) {
            alert('页码输入不正确，请重新输入');
        }else {
            this.args.current = Number(num);
            this.fillHtml();
        }
    }


}
ctrl.$inject=['$timeout','$window'];
module.exports = angular.module('page', [])
    .component('page', {
        template: tem,
        controller: ctrl,
        controllerAs: 'pageCtrl',
        bindings:{
            styleOption:'<',
            args:'<'
        }
    })
    .name;