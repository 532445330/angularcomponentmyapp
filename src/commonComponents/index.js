/**
 * Created by wanglianxin on 2018/11/7.
 */
let dateSelelct = require('./dateSelect');
let dateTem = require('./dateTem');
let tableList = require('./table');
let barCommon = require('./barCommon');
let page = require('./page');
let chatItem=require('./chatItem');

module.exports = angular.module('commonComponents',[
    dateSelelct,
    tableList,
    barCommon,
    dateTem,
    page,
    chatItem
]).name;