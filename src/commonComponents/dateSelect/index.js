/**
 * Created by wanglianxin on 2018/11/7.
 */
let tem = require('./dateSelect.html');
require('./dateSelect.scss');
let laydate = require('./laydate');
// laydate.path=false;
require('./theme/default/laydate.css');
require('./theme/default/font/iconfont.eot');
require('./theme/default/font/iconfont.svg');
require('./theme/default/font/iconfont.ttf');
require('./theme/default/font/iconfont.woff');

class ctrl{
    constructor(){
        let _this = this;
        this.date='';
        setTimeout(()=>{
            // console.log(_this.data);
        },0);

    }
}
class directive{
    constructor(timeout,$state,publicFn,service){
        return{
            require: '?ngModel',
            restrict: 'A',
            scope: {
                ngModel: '=', //= 设置可以接收数据绑定
                maxDate: '@',  //@ 使用来自父范围的数据绑定
                minDate: '@'      //& 创建一个委托函数
            },
            link: function (scope, element, attr, ngModel) {
                let _config = {};
                // 渲染模板完成后执行
                timeout(()=> {
                    // 初始化参数
                    var dateType='datetime',format='yyyy-MM-dd HH:mm:ss';
                    if(attr.datetype=='time'){
                        dateType='time';
                        format='HH:mm:ss';
                    }
                    else if(attr.datetype=='date'){
                        dateType='date';
                        format='yyyy-MM-dd';
                    }
                    _config = {
                        elem: '#' + attr.id,
                        type: dateType,
                        theme:'#4aa0e9',
                        format: format,
                        range: '~',
                        done: function (value, date,endDate) {
                            let timeData={
                                start:`${date.year}-${publicFn.toDou(date.month)}-${publicFn.toDou(date.date)}`,
                                end:`${date.year}-${publicFn.toDou(endDate.month)}-${publicFn.toDou(endDate.date)}`
                            };

                            service.setDate(attr.optionName,timeData);
                            $state.go(attr.stateUrl,{[attr.optionName]:JSON.stringify(timeData)});
                            scope.$apply(function (){
                                setViewValue(value);
                            });
                        }
                    };
                    // if(attr.data=='delay'){
                    //     if(attr.datetype=='datetime'){
                    //         _config.min = attr.minDate;
                    //         _config.max = attr.maxDate;
                    //     }
                    //     if(attr.ngModel=='stopDate'){
                    //         _config.value = attr.maxDate;
                    //     }
                    //     else if(attr.ngModel=='startDate'){
                    //         _config.value = attr.minDate;
                    //     }
                    //     else if(attr.ngModel=='timePoint'){
                    //         _config.value = attr.minDate;
                    //     }
                    //     _config.btns=['clear', 'confirm'];
                    // }

                    // 初始化
                    laydate.render(_config);
                    // setViewValue(_config.value);
                    // console.log(attr);


                    // 更新模型上的视图值
                    function setViewValue(val){
                        ngModel.$setViewValue(val);
                    }
                }, 0);
            }
        }
    }
}
directive.$inject = ['$timeout','$state','publicFn','service'];
module.exports = angular.module('dateSelect',[])
.component('dateSelect',{
    template:tem,
    controller:ctrl,
    controllerAs:'dateSelectCtrl',
    bindings:{
        data:'<'
    }
})
    .directive('ngtLaydate',directive)
    .name;