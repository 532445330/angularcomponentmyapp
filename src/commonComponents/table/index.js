/**
 * Created by wanglianxin on 2018/11/10.
 */
let tem = require('./table.html');
require('./style.scss');
class ctrl {
    constructor() {
        // setTimeout(()=>{
        //     console.log(
        //         this.tableClassName,
        //         this.showThead,
        //         this.tableHeadList,
        //         this.tableBodyList,
        //         this.tableWidth,
        //         this.tableBg,
        //         this.trLineHeight,
        //         this.trBorderB,
        //         this.isInterval,
        //         this.theadBg,
        //         this.trEvenBg,
        //         this.trOddBg,
        //         this.trPadding
        //     )
        // },0);
    }
}
module.exports = angular.module('tableList', [])
    .component('tableList',{
        template:tem,
        controller:ctrl,
        controllerAs:'tableListCtrl',
        bindings:{
            tableClassName:"<",
            showThead:"<",
            tableHeadList:"<",
            tableBodyList:"<",
            tableWidth:"<",
            tableBg:"<",
            trLineHeight:"<",
            trBorderB:"<",
            isInterval:"<",
            theadBg:"<",
            trEvenBg:"<",
            trOddBg:"<",
            tdPadding:"<",
            trPadding:"<"
        }
    })
    .name;
// tableClassName:"<",   //table的class名
//     showThead:'<',         //是否显示thead
//     tableHeadList:'<',       //table head数据
//     tableBodyList:'<',         //table body数据
//     tableWidth:'<',             //table 宽
//     tableBg:'<',                //背景
//     trLineHeight:'<',            //tr的lineheight
//     trBorderB:'<',                 //tr的border-bottom
//     isInterval:'<',                 //是否有间隔
//     theadBg:'<',                       //thead背景
//     trEvenBg:'<',                    //偶数
//     trOddBg:'<',                     //奇数